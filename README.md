# uWSGI

Hosting services. https://en.m.wikipedia.org/wiki/UWSGI

# Official documentation
* [*The uWSGI project*](https://uwsgi-docs.readthedocs.io)

# Unofficial documentation
* [*uWSGI*](https://en.m.wikipedia.org/wiki/UWSGI)
* quasi-identical to SCGI
  * [*A Comparison of Web Servers for Python Based Web Applications*
    ](https://www.digitalocean.com/community/tutorials/a-comparison-of-web-servers-for-python-based-web-applications)
    2013-10-28 O.S Tezer

# Web servers
* Included in Apache and Nginx
* [*Module Apache mod_proxy_uwsgi*
  ](https://httpd.apache.org/docs/2.4/mod/mod_proxy_uwsgi.html)
* https://packages.debian.org/bullseye/amd64/apache2-bin/filelist

# Web Application able to use uWSGI
## Erlang/OTP
* [*Integrating uWSGI with Erlang*
  ](https://uwsgi-docs.readthedocs.io/en/latest/Erlang.html)

## Python
### Paste-compatible
* [Pyramid](https://docs.pylonsproject.org/projects/pyramid-cookbook/en/latest/deployment/uwsgi.html)

### ASGI
* asgi uwsgi
...

## Rust
* rust uwsgi
...